//
//  FruitIAPHelper.m
//  BuyFruit
//
//  Created by Michael Beyer on 16.09.13.
//  Copyright (c) 2013 Michael Beyer. All rights reserved.
//

#import "MyIAPHelper.h"

static NSString *kIdentifierAB    = @"com.kraftwerking.animalbuddies.animalbuddies";


@implementation MyIAPHelper

// Obj-C Singleton pattern
+ (MyIAPHelper *)sharedInstance {
    static MyIAPHelper *sharedInstance;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        NSSet *productIdentifiers = [NSSet setWithObjects:
                                     kIdentifierAB,
                                     nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

- (NSString *)imageNameForProduct:(SKProduct *)product
{
    if ([product.productIdentifier isEqualToString:kIdentifierAB]) {
        return @"image_apple";
    }
       return nil;
}

- (NSString *)descriptionForProduct:(SKProduct *)product
{

       return nil;
}

@end
