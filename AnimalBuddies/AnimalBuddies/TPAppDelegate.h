//
//  TPAppDelegate.h
//  AnimalBuddies
//
//  Created by ellisa on 7/7/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;
#import "iRate.h"

@interface TPAppDelegate : UIResponder <UIApplicationDelegate>
{
    
    UIImageView *splashImage;
    
}

@property (strong, nonatomic) UIWindow *window;


- (void)startupAnimationDone:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;
- (void)buttonGridTapped;
@end
